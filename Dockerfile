FROM centos:centos7

RUN echo "Europe/Moscov" > /etc/timezone
RUN localedef -i ru_RU -f UTF-8 ru_RU.UTF-8

ENV GOSU_VERSION 1.10
RUN set -x && \
    curl -Lo /usr/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-amd64" && \
    curl -Lo /tmp/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-amd64.asc" && \
    export GNUPGHOME="$(mktemp -d)" && \
    for server in $(shuf -e ha.pool.sks-keyservers.net \
                            hkp://p80.pool.sks-keyservers.net:80 \
                            keyserver.ubuntu.com \
                            hkp://keyserver.ubuntu.com:80 \
                            pgp.mit.edu) ; do \
        gpg --keyserver "$server" --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 && break || : ; \
    done && \
    gpg --batch --verify /tmp/gosu.asc /usr/bin/gosu && \
    rm -r "$GNUPGHOME" /tmp/gosu.asc && \
    chmod +x /usr/bin/gosu && \
    # Allow user to become root
    #chmod u+s /usr/bin/gosu && \
    gosu nobody true

RUN yum install -y epel-release yum-utils \
	&& yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm \
	&& yum-config-manager --enable remi-php73 \
	&& yum install -y php php-common php-opcache php-mcrypt php-cli php-gd php-curl php-mysqlnd \
	&& yum clean all

RUN yum install -y ruby-devel gcc make rpm-build rubygems \
	&& gem install --no-ri --no-rdoc fpm \
	&& yum remove -y gcc make rpm-build \
	&& yum clean all

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer